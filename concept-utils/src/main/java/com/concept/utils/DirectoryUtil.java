/**
 * 
 */
package com.concept.utils;

import java.io.File;

/**
 * @author f3557790 <sabelo.simelane@fnb.co.za>
 */
public class DirectoryUtil {

	
	public static String currentDir() {
		File currDir = new File(".");
	    String path = currDir.getAbsolutePath();
	    //path.replaceAll("\\", "/");
	    
	    path = path.substring(0, path.length()-2);
	    System.out.println("path:"+path);
	    
	    return path;
	}
}
