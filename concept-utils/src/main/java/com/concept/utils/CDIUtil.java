/**
 * 
 */
package com.concept.utils;

import java.io.Serializable;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

/**
 * @author F3557790
 * 
 */
public class CDIUtil implements Serializable{

	private static final long serialVersionUID = 1L;
	private @Inject	BeanManager bm;

	@SuppressWarnings("unchecked")
	public <T> Object newInstance(T t, String className) throws ClassNotFoundException {

		Bean<?> bean = (Bean<?>) bm.getBeans(Class.forName(className)).iterator().next();
		CreationalContext<?> ctx = bm.createCreationalContext(bean);
		T instance = (T) bm.getReference(bean, Class.forName(className), ctx);

		return instance;
	}
}
