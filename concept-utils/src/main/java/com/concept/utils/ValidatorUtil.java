package com.concept.utils;

import java.util.Date;

import com.concept.utils.exception.ValidationException;

/**
 * 
 * @author Sabside
 *
 */
public class ValidatorUtil {

	public static void checkNull(Object object, String message) throws ValidationException {
		if (object == null) throw new ValidationException(message);
	}
	
	public static void checkNull(Object object, String message, String fieldId, String fieldDisplayName) throws ValidationException {
		if (object == null) throw new ValidationException(message, fieldDisplayName, fieldId);
	}
	
	public static void checkEmptyNull(String string, String message) throws ValidationException {
		if (string == null || string.isEmpty()) throw new ValidationException(message);
	}
	
	public static void checkEmptyNull(Date date, String message) throws ValidationException {
		if (date == null) throw new ValidationException(message);
	}
	
	public static void checkEmptyNull(String string, String message, String fieldId, String fieldDisplayName) throws ValidationException {
		if (string == null || string.isEmpty()) throw new ValidationException(message, fieldDisplayName, fieldId);
	}
	
	public static void checkEmptyNull(int input, String message) throws ValidationException {
		if (input == 0) throw new ValidationException(message);
	}
	
	public static void checkEmptyNull(int input, String message, String fieldId, String fieldDisplayName) throws ValidationException {
		if (input == 0) throw new ValidationException(message, fieldDisplayName, fieldId);
	}
}
