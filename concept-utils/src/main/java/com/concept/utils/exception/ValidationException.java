package com.concept.utils.exception;

/**
 * 
 * @author Sabside
 *
 */
public class ValidationException extends Exception {

	private static final long serialVersionUID = 1L;
	private String messageStr;
	private String fieldDisplayName;
	private String fieldId;

	public ValidationException(String messageStr, String fieldDisplayName, String fieldId) {
		super();
		this.messageStr = messageStr;
		this.fieldDisplayName = fieldDisplayName;
		this.fieldId = fieldId;
	}

	public ValidationException() {
		super();

	}

	public ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public ValidationException(String message, Throwable cause) {
		super(message, cause);

	}

	public ValidationException(String message) {
		super(message);

	}

	public ValidationException(Throwable cause) {
		super(cause);

	}

	public String getMessageStr() {
		return messageStr;
	}

	public String getFieldDisplayName() {
		return fieldDisplayName;
	}

	public String getFieldId() {
		return fieldId;
	}

}
