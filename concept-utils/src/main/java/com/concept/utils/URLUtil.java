package com.concept.utils;

import static java.net.HttpURLConnection.HTTP_ACCEPTED;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_GONE;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

/**
 * 
 * @author Sabside
 *
 */
public class URLUtil {

	private static final Log log = LogFactory.getLog(URLUtil.class);
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String APPLICATION_JSON = "application/json"; 
	private static final String METHOD_GET = "GET"; 
	private static final int bufferSize = 8192;
	private static final String METHOD_PUT = "PUT"; 
	private static final String METHOD_POST = "POST"; 
	private static final String METHOD_DELETE = "DELETE";
	private static final String METHOD_PATCH = "PATCH";
	
	public static MultipartEntityBuilder createMultipartBuilder() {
		return MultipartEntityBuilder.create();
	}
	
	public static String callMultipart(String uri, MultipartEntityBuilder builder, Map<String, String> headers) throws Exception {
		HttpEntity data = builder.build();
		HttpUriRequest request = RequestBuilder
                .put(uri)
                .setEntity(data)
                .build();
		headers.forEach((k, v)->{
			request.addHeader(k, v);
		});
		
		log.info("Executing request " + request.getRequestLine());
		
		ResponseHandler<String> responseHandler = resp -> {
            int status = resp.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = resp.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }
        };
        
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String responseBody = null;
		responseBody = httpclient.execute(request, responseHandler);

		log.debug("Executing request " + request.getRequestLine());
		
        return responseBody;
	}
	
	public static Map<String, List<String>> splitQuery(String url) throws UnsupportedEncodingException {
		final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
		final String[] pairs = url.split("&");
		for (String pair : pairs) {
			final int idx = pair.indexOf("=");
			final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
			if (!query_pairs.containsKey(key)) {
				query_pairs.put(key, new LinkedList<String>());
			}
			final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
			query_pairs.get(key).add(value);
		}
		return query_pairs;
	}
	
	public static String get(final String uri, final Object params, Map<String, String> headers) throws Exception {
		HttpURLConnection request = createGet(uri, headers);
		return sendJson(request, params);
	}
	
	public static String delete(final String uri, final Object params, Map<String, String> headers) throws Exception {
		HttpURLConnection request = createDelete(uri, headers);
		return sendJson(request, params);
	}
	
	public static String post(final String uri, final Object params, Map<String, String> headers) throws Exception {
		HttpURLConnection request = createPost(uri, headers);
		return sendJson(request, params);
	}
	
	public static String patch(final String uri, final Object params, Map<String, String> headers) throws Exception {
		HttpURLConnection request = createPatch(uri, headers);
		return sendJson(request, params);
	}
	
	public static String put(final String uri, final Object params, Map<String, String> headers) throws Exception {
		HttpURLConnection request = createPut(uri, headers);
		return sendJson(request, params);
	}
	
	private static String sendJson(final HttpURLConnection request, final Object params) throws IOException {
		sendParams(request, params);
		return getResponse(request);
	}
	
	public static String getResponse(final HttpURLConnection request) throws IOException {
		final int code = request.getResponseCode();
		//updateRateLimits(request);
		if (isOk(code)) {
				String response = IOUtils.toString(getStream(request), StandardCharsets.UTF_8.name());
				log.debug("response: "+response);
				return response;
			} else
			if (isEmpty(code))
				return null;
		throw createException(getStream(request), code,
				request.getResponseMessage());
	}
	
	private static IOException createException(InputStream response, int code, String status) {
		try {
			return new IOException(IOUtils.toString(response, StandardCharsets.UTF_8.name()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new IOException();
	}
	
	private static boolean isEmpty(final int code) {
		return HTTP_NO_CONTENT == code;
	}
	
	private static boolean isError(final int code) {
		switch (code) {
		case HTTP_BAD_REQUEST:
		case HTTP_UNAUTHORIZED:
		case HTTP_FORBIDDEN:
		case HTTP_NOT_FOUND:
		case HTTP_CONFLICT:
		case HTTP_GONE:
		case 422:
		case HTTP_INTERNAL_ERROR:
			return true;
		default:
			return false;
		}
	}
	
	private static InputStream getStream(HttpURLConnection request) throws IOException {
		if (request.getResponseCode() < HTTP_BAD_REQUEST)
			return request.getInputStream();
		else {
			InputStream stream = request.getErrorStream();
			return stream != null ? stream : request.getInputStream();
		}
	}
	
	private static boolean isOk(final int code) {
		switch (code) {
		case HTTP_OK:
		case HTTP_CREATED:
		case HTTP_ACCEPTED:
			return true;
		default:
			return false;
		}
	}
	
	public static HttpURLConnection createPatch(String uri, Map<String, String> headers) throws IOException {
		return createConnection(uri, METHOD_PATCH, headers);
	}
	
	public static HttpURLConnection createDelete(String uri, Map<String, String> headers) throws IOException {
		return createConnection(uri, METHOD_DELETE, headers);
	}
	
	private static HttpURLConnection createGet(String uri, Map<String, String> headers) throws IOException {
		return createConnection(uri, METHOD_GET, headers);
	}
	
	public static HttpURLConnection createPost(String uri, Map<String, String> headers) throws IOException {
		return createConnection(uri, METHOD_POST, headers);
	}
	
	public static HttpURLConnection createPut(String uri, Map<String, String> headers) throws IOException {
		return createConnection(uri, METHOD_PUT, headers);
	}
	
	protected static HttpURLConnection createConnection(String uri) throws IOException {
		URL url = new URL(uri);
		return (HttpURLConnection) url.openConnection();
	}
	
	private static HttpURLConnection configureRequest(final HttpURLConnection request, Map<String, String> headers) {
		if (headers !=null) {
			headers.forEach((key, val)->{
				request.setRequestProperty(key, val);
			});
		}
		return request;
	}
	
	private static HttpURLConnection createConnection(String uri, String method, Map<String, String> headers) throws IOException {
		log.debug(String.format("calling: [%s][%s]", method, uri));
		HttpURLConnection connection = createConnection(uri);
		connection.setRequestMethod(method);
		return configureRequest(connection, headers);
	}
	
	
	private static String toJson(Object object) throws IOException {
		try {
			return new Gson().toJson(object);
		} catch (JsonParseException jpe) {
			IOException ioe = new IOException(
					"Parse exception converting object to JSON"); //$NON-NLS-1$
			ioe.initCause(jpe);
			throw ioe;
		}
	}
	
	public static void sendParams(HttpURLConnection request, Object params) throws IOException {
		
		if (params != null) {
			request.setDoOutput(true);
			byte[] data = null;
			if (params instanceof String) {
				data = ((String) params).getBytes(StandardCharsets.UTF_8);
			} else if (params instanceof JSONObject) {
				data = ((JSONObject)params).toString().getBytes(StandardCharsets.UTF_8);
			} else {
				data = new Gson().toJson(params).getBytes(StandardCharsets.UTF_8);
			}
			
			log.info("inputdata: "+new String(data));
			DataOutputStream output = new DataOutputStream(request.getOutputStream());
			//request.setFixedLengthStreamingMode(data.length);
			//BufferedOutputStream output = new BufferedOutputStream(request.getOutputStream(), bufferSize);
			try {
				output.write(data);
				output.flush();
			} finally {
				try {
					output.close();
				} catch (IOException ignored) {
					// Ignored
				}
			}
		} 
	}
	
}
