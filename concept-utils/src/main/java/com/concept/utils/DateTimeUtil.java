/**
 * 
 */
package com.concept.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.ocpsoft.prettytime.PrettyTime;

/**
 * @author Sabelo Simelane <sabside@gmail.com>
 * 
 */
public class DateTimeUtil {

	public static final String yyyy_MM_dd = "yyyy-MM-dd";
	public static final String yyyy_MM_dd_HH_mm = "yyyy-MM-dd HH:mm";
	public static final String yyyy_MM_dd_HH_mm_ss_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String yyyy_MM_ddTHH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String PRETTY = "pretty";
	public static final String PRETTY_SHORT = "pretty_short";

	
	/**
	 * "Sun Jun 04 09:00:00 CAT 2017"
	 */
	public static final String EEE_MMM_dd_hh_mm_ss_zzzz_yyyy = "EEE MMM dd HH:mm:ss zzzz yyyy";

	/**
	 * Based on format yyyy-MM-dd
	 */
	public static Date fromString(String strDate) throws ParseException{
		return fromString(strDate, yyyy_MM_dd);
	}
	
	public static Date fromString(String strDate, String format) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.parse(strDate);
	}
	
	/**
	 * Based on format yyyy-MM-dd
	 */
	public static String toString(Date date) throws ParseException {
		return toString(date, yyyy_MM_dd);
	}
	
	public static String toString(Date date, String format) throws ParseException {
		
		String dateString = null;
		if (format.equals(PRETTY_SHORT)) {
		
			PrettyTime pretty = new PrettyTime();
			return  pretty.format(date);
		} else if (format.equals(PRETTY)) {
			if (isToday(new Date(), date)) {
				return "Today at "+toString(date, "HH:mm");
			}
			
			PrettyTime pretty = new PrettyTime();
			dateString = String.format("%s at %s - %s", pretty.format(date), toString(date, "HH:mm"), toString(date, "dd.MM.yyyy"));
			return dateString;
		}
		
		SimpleDateFormat sdfr = new SimpleDateFormat(format);

		try {
			dateString = sdfr.format(date);
		} catch (Exception ex) {
			return null;
		}
		
		return dateString;
	}
	
	public static boolean isToday(Date first, Date other) throws ParseException {
		return toString(first).equals(toString(other));
	}
	
	/**
	 * Will return today's date with hours & minutes set to 0. i.e 12 am
	 */
	public static Date getBeginningOfDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	/**
	 * Calculates the difference between two dates
	 * 
	 * unit is Calendar unit e.g. Calendar.MINUTE
	 */
	public static double difference(Date from, Date to, int unit){
		long diffInMillis = to.getTime() - from.getTime();
		
		if (unit == Calendar.SECOND){
			return (diffInMillis / 1000);
			
		} else if (unit == Calendar.MINUTE){
			return ((diffInMillis / 1000) / 60.00);
			
		} else if (unit == Calendar.HOUR){
			return (((diffInMillis / 1000) / 60.00)/ 60.00);
			
		} else if (unit == Calendar.DAY_OF_YEAR){
			return (((diffInMillis / 1000) / 60.00)/ 60.00)/24.00;
		}
		
		return -1;
	}
}
