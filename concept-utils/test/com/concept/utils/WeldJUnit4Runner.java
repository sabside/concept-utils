package com.concept.utils;


import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

/**
 * 
 * @author Sabside
 *
 */
public class WeldJUnit4Runner extends BlockJUnit4ClassRunner {

	public WeldJUnit4Runner(Class<?> klass) throws InitializationError {
		super(klass);
	}

	@Override
    protected Object createTest() {
        final Class<?> test = getTestClass().getJavaClass();
        return WeldContext.INSTANCE.getBean(test);
    }
}
