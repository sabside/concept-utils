/**
 * 
 */
package com.concept.utils;

import java.text.ParseException;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Sabelo Simelane <sabside@gmail.com>
 * 
 */
public class DateTimeUtilTest {

	@Test
	public void test() throws ParseException {

		String strDate = "1978-09-01 04:00:00";
		Date date = DateTimeUtil.fromString(strDate);
		System.out.println(date);
		
		Assert.assertTrue(date.toString().contains("Fri"));
	}

}
